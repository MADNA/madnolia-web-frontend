import { Injectable } from '@angular/core';

import { environment } from "../../environments/environment";

const base_url = environment.base_url

@Injectable({
  providedIn: 'root'
})
export class FileUploadService {

  constructor() { }

  async updateImg(
    file:File,
    type: 'users',
    id: string
  ){

    try {
      
      const url = `${base_url}/upload/${ type }`
      const formData = new FormData();
      formData.append('img', file)

      const resp = await fetch(url, {
        method: 'PUT',
        headers: {
          'token': localStorage.getItem('token') || ''
        },
        body: formData
      })
      
      const data = await resp.json()

      return data

    } catch (error) {
      return error
    }
  }
}
