import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

const api_key = "8af7cb7fc9d949acac94ab83be57ed1b"
const base_url = environment.base_url
const api_url = "https://api.rawg.io/api"

@Injectable({
  providedIn: 'root'
})
export class GamesService {

  constructor(private http:HttpClient) { }

  getGamesByPlatform = (platform,page) =>{
    return this.http.get(`${api_url}/games`, {
      params: {
        platforms: platform,
        tags: 'online, multiplayer',
        page_size: '9',
        page,
        key: api_key
      }
    })
  }

  resizeImages(games){
    games.forEach(game => {
      let image = game.background_image.split('/')
      if(image[image.length -3] == "screenshots"){
        game.background_image = `https://media.rawg.io/media/crop/600/400/screenshots/${image[image.length - 2]}/${image[image.length - 1]}`
      }else{

        game.background_image = `https://media.rawg.io/media/crop/600/400/games/${image[image.length - 2]}/${image[image.length - 1]}`
      }
    });
  }

  searchGamesByPlatform(game_name:string, platform_id){
    return this.http.get(`${api_url}/games`,{
      params:{
        key: api_key,
        search: game_name,
        platforms: platform_id,
        page_size: "5"
      }
    })
  }

  getGame = (id) =>{

    return this.http.get(`${api_url}/games/${id}`, {
      params:{
        // id,
        key: api_key
      }
    })

  }
}
