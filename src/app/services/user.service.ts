import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { tap, map, catchError } from "rxjs/operators";
import { environment } from 'src/environments/environment';
import { RegisterForm } from '../auth/interfaces/register-form.interface';
import { LoginForm } from '../auth/interfaces/login-form.interface';
import { Observable, of } from 'rxjs';

import { User } from '../models/user.model';

const base_url = environment.base_url
const api_key = "8af7cb7fc9d949acac94ab83be57ed1b"

@Injectable({
  providedIn: 'root'
})
export class UserService {

  public user:any

  constructor(private http:HttpClient) { }

  validateToken(): Observable<boolean> {
    const token = localStorage.getItem('token') || ''

    return this.http.get(`${base_url}/renew`, {
      headers:{
      'token': token
      }
    }).pipe(
      tap( (resp:any) => {
        localStorage.setItem('token', resp.token)
        const {name, username, email, platforms, acceptInvitations, _id, img, notifications, thumb_img} = resp.user
        this.user = new User(name, username, email, platforms, acceptInvitations, notifications, _id, img, thumb_img)
      }),
      map(resp => true),
      catchError( error => of(false))
    )

  }

  verifyUserName(username:string, email:String){
    return this.http.post(`${base_url}/verify_user/${username}/${email}`, username)
  }

  createUser(formData:RegisterForm){
    return this.http.post(`${ base_url }/signin`, formData).pipe(
      tap( (resp:any) => {
        localStorage.setItem('token', resp.token)
      })
    )
  }

  login( loginForm:LoginForm ){

    return this.http.post(`${base_url}/login`, loginForm).pipe(
      tap( (resp:any) =>{
        localStorage.setItem('token', resp.token)
      })
    )
  }

  userInfo(){
    return this.http.get(`${base_url}/user_info`, {
      headers: {
        'token': localStorage.getItem('token')
      }
    })
  }

  userPlatforms() {
    return this.http.get(`${base_url}/user_platforms`,{
      headers:{
        "token": localStorage.getItem('token')
      }
    })
  }

  updateUser(userData){
    return this.http.put(`${base_url}/update_user`, userData,{
      headers:{
        'token': localStorage.getItem('token')
      }
    }).pipe(
      tap( (resp:any) => {
        const {name, username, email, acceptInvitations} = resp

        this.user.name = name
        this.user.username = username
        this.user.email = email
        this.user.acceptInvitations = acceptInvitations
      })
    )
  }

  updateUserPlatforms(platforms){
    return this.http.put(`${base_url}/update_user_platforms`, platforms,{
      headers:{
        'token': localStorage.getItem('token')
      }
    })
  }

  searchPartners(username:String){
    return this.http.get(`${base_url}/search_user/${username}`)
  }

  getPartners(){
    return this.http.get(`${base_url}/get_partners`,{
      headers:{
        'token': localStorage.getItem('token')
      }
    })
  }

  addPartner(id){
    return this.http.post(`${base_url}/add_partner`,id, {
      headers:{
      'token': localStorage.getItem('token')
    }
    })
  }

  resetNofications(){
    return this.http.get(`${base_url}/reset_notifications`,{
      headers:{
        'token': localStorage.getItem('token')
      }
    })
  }

  getInvitations(){
    return this.http.get(`${base_url}/invitations`, {
      headers:{
        "token": localStorage.getItem('token')
      }
    }).pipe(
      map((resp:any)=> resp.invitations)
    )

  }
  
}
