export class Match{
    constructor(
        message:String,
        active:boolean,
        _id:string,
        game_name:String,
        game_id:String,
        platform:Number,
        date:Date,
        user:String,
        likes?:[],
        img?:String
    ){}
}