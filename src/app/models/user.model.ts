import { environment } from "../../environments/environment";

const base_url = environment.base_url

export class User{

    constructor(
        public name: string,
        public username: string,
        public email: string,
        public platforms: Array<any>,
        public acceptInvitations: string,
        public notifications: number,
        public uid: string,
        public img?:string,
        public thumb_img?:string
    ){}
      

}