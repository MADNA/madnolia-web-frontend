export class Platform{
    constructor(
        public name: String,
        public id: Number,
        public platform_ico: String,
        public games?: Array<any>
            // {
            //     name?: String,
            //     amount?: Number
            //     img?: String,
            //     id? : Number
            // }
        // ]
    ){}
    get getGames (){
        return this.games
    }
    push_game(game){
        this.games.push(game)
        // var actual_games = this.games
        // actual_games.push(game)
        // this.games = actual_games
    }
}