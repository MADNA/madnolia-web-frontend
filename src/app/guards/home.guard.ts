import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { UserService } from '../services/user.service';

@Injectable({
  providedIn: 'root'
})
export class HomeGuard implements CanActivate {

  constructor(private userService:UserService, private router:Router){}

  canActivate() {

      if(localStorage.getItem('token')){
        this.router.navigateByUrl('/home')
      }else{
        return true
      }
  }
  
}
