import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { NintendoComponent } from './nintendo/nintendo.component';
import { PcComponent } from './pc/pc.component';
import { PlaystationComponent } from './playstation/playstation.component';
import { XboxComponent } from './xbox/xbox.component';
import { PhoneComponent } from './phone/phone.component';
import { PlatformsComponent } from './platforms.component';
import { BrowserModule } from '@angular/platform-browser';
import { PlatformComponent } from './platform/platform.component';
import { TranslateModule } from '@ngx-translate/core';



@NgModule({
  declarations:
  [NintendoComponent,
    PcComponent,
    PlaystationComponent,
    XboxComponent,
    PhoneComponent,
    PlatformsComponent,
    PlatformComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule,
    TranslateModule
  ],
  exports:[
    NintendoComponent,
    PcComponent,
    PlaystationComponent,
    XboxComponent,
    PhoneComponent,
    PlatformsComponent
  ]
})
export class PlatformsModule { }
