import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GamesService } from 'src/app/services/games.service';
import { PlatformService } from 'src/app/services/platform.service';

@Component({
  selector: 'app-platform',
  templateUrl: './platform.component.html',
  styleUrls: ['./platform.component.css']
})
export class PlatformComponent implements OnInit {

  @ViewChild('gamesContainer')
  gamesElement:ElementRef

  platformId:Number
  games = []
  page = 1
  platformName:String


  constructor(
    private activatedRoute:ActivatedRoute,
    private gamesService:GamesService,
    private platformsService:PlatformService
    ) { 
    this.activatedRoute.params.subscribe(params => {
      this.platformId = params.platform
      this.platformsService.getPlatformInfo(params.platform).subscribe((resp:any) => {
        this.platformName = resp.name
      })
    })
  }

  ngOnInit(): void {
    this.LoadGames(this.page)
  }

  LoadGames = async(page) =>{
    await this.gamesService.getGamesByPlatform(this.platformId,page).subscribe((resp:any) =>{
     this.gamesService.resizeImages(resp.results)
    resp.results.forEach(element => {
      this.games.push(element)
    });
     this.page += 1
   }, (err) => {
       console.log(err)
     })

  }

  verScroll(value){
    // console.log(value)
    let pos = this.gamesElement.nativeElement.scrollTop + this.gamesElement.nativeElement.offsetHeight
    let max = this.gamesElement.nativeElement.scrollHeight
    if(pos >= max){
      this.LoadGames(this.page)
    }
  }
  

}
