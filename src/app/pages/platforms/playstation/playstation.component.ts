import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from "@angular/common";
import { DOCUMENT } from '@angular/common';

declare function verifyChecked(id:string)

@Component({
  selector: 'app-playstation',
  templateUrl: './playstation.component.html',
  styleUrls: ['./playstation.component.css']
})
export class PlaystationComponent implements OnInit, OnDestroy  {


  constructor(@Inject(DOCUMENT) private document:Document) {
    console.log(this.document.location.href)
   }

  link = this.document.location.href
  urlParts = this.link.split('/')
  path = this.urlParts[this.urlParts.length - 2]

  showRegister = () => this.path == 'register'
  ngOnInit(): void {
  }
  ngOnDestroy(): void {
  }
}
