import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';

import { GamesService } from 'src/app/services/games.service';

@Component({
  selector: 'app-phone',
  templateUrl: './phone.component.html',
  styleUrls: ['./phone.component.css']
})
export class PhoneComponent implements OnInit {

  @ViewChild('gamesContainer')
  gamesElement:ElementRef

  platformId:Number = 21
  games = []
  page = 1
  platformName:String
  constructor(private gamesService:GamesService) {this.LoadGames(this.page)}

  ngOnInit(): void {
  }

  LoadGames = async(page) =>{
    await this.gamesService.getGamesByPlatform(this.platformId,page).subscribe((resp:any) =>{
     this.gamesService.resizeImages(resp.results)
    resp.results.forEach(element => {
      this.games.push(element)
    });
     this.page += 1
   }, (err) => {
       console.log(err)
     })

  }

  verScroll(value){
    // console.log(value)
    let pos = this.gamesElement.nativeElement.scrollTop + this.gamesElement.nativeElement.offsetHeight
    let max = this.gamesElement.nativeElement.scrollHeight
    if(pos >= max){
      this.LoadGames(this.page)
    }
  }
}
