import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { GamesService } from 'src/app/services/games.service';

@Component({
  selector: 'app-pc',
  templateUrl: './pc.component.html',
  styleUrls: ['./pc.component.css']
})
export class PcComponent implements OnInit {

  @ViewChild('gamesContainer')
  gamesElement:ElementRef

  platformId:Number = 4
  games = []
  page = 1
  platformName:String
  constructor(private gamesService:GamesService) {this.LoadGames(this.page)}

  ngOnInit(): void {
  }

  LoadGames = async(page) =>{
    await this.gamesService.getGamesByPlatform(this.platformId,page).subscribe((resp:any) =>{
     this.gamesService.resizeImages(resp.results)
    resp.results.forEach(element => {
      this.games.push(element)
    });
     this.page += 1
   }, (err) => {
       console.log(err)
     })

  }

  verScroll(value){
    // console.log(value)
    let pos = this.gamesElement.nativeElement.scrollTop + this.gamesElement.nativeElement.offsetHeight
    let max = this.gamesElement.nativeElement.scrollHeight
    if(pos >= max){
      this.LoadGames(this.page)
    }
  }
}
