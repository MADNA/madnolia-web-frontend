import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.css']
})
export class NotificationsComponent implements OnInit {

  constructor(private userService:UserService) { }

  matches:Array<any>

  ngOnInit(): void {
    this.userService.getInvitations().subscribe(resp =>{
      this.matches = resp
      this.matches.forEach(element => {
        element.date = new Date(element.date)
      });
    })
    if(this.userService.user){
      if(this.userService.user.notifications > 0){
        this.userService.resetNofications().subscribe(()=>this.userService.user.notifications = 0)
      }
    }
  }

}
