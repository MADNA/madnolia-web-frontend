import { Component, Injectable, OnInit, ViewChild, ElementRef, OnDestroy} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Socket } from 'ngx-socket-io';

import { environment } from "../../../environments/environment";

import { MatchService } from 'src/app/services/match.service';
import { PlatformService } from 'src/app/services/platform.service';

@Component({
  selector: 'app-match',
  templateUrl: './match.component.html',
  styleUrls: ['./match.component.css']
})
@Injectable()

export class MatchComponent implements OnInit, OnDestroy{
  
  @ViewChild('container', {static: false}) container: ElementRef
  match
  platform
  loading = true
  message:string
  messages:Array<any>


  constructor(
    private matchService:MatchService,
    private activatedRoute:ActivatedRoute,
    private platformService:PlatformService,
    private socket:Socket) { 
    this.activatedRoute.params.subscribe(params => {

      this.matchService.getMatch(params.match).subscribe((resp:any) =>{
        this.match = resp.match
        
        this.socket.emit('init_match_chat', resp.match._id)
        this.socket_inputs()
        this.match.date = new Date(this.match.date)

        this.platformService.getPlatformInfo(this.match.platform).subscribe(platform =>{
          this.platform = platform
          this.loading = false
          this.messages = resp.match.chat
          this.scrollToBottom()
        })

      }, (err) => console.error(err))
    })
  }
  
  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.socket.emit('disconnect_chat')
  }

   socket_inputs(){
    this.socket.on('message', (message) =>{
      if(message.room == this.match._id){

        this.messages.push(message)
        this.scrollToBottom()      
      }
    })
  }
  
  scrollToBottom(): void{
    try {
      setTimeout(() => {
        this.container.nativeElement.scrollTop = this.container.nativeElement.scrollHeight;
      }, 10);
    } catch (err) {
      console.log(err)
    }
  }

  onSubmit(event){
    if(event){
      if(event.key != "Enter"){
        return 
      }else{
        event.preventDefault()
      }
    }

    if(this.message === "" || !this.message){
      return
    }
    this.socket.emit('message', {"message": this.message, "room": this.match._id})
    this.message = null
  }


}
