import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';

import { NewComponent } from './new.component';
import { NewPageComponent } from './new-page/new-page.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CreateMatchComponent } from './match/create-match.component';
import { TournamentComponent } from './tournament/tournament.component';



@NgModule({
  declarations: [
    NewComponent,
    NewPageComponent,
    CreateMatchComponent,
    TournamentComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,

  ],
  exports:[
    CreateMatchComponent,
    TournamentComponent,
    NewComponent,
    NewPageComponent,
    TranslateModule
  ]
})
export class NewModule { }
