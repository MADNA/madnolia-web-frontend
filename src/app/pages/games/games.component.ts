import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Match } from 'src/app/auth/interfaces/match.interface';
import { GamesService } from 'src/app/services/games.service';
import { MatchService } from 'src/app/services/match.service';
import { PlatformService } from 'src/app/services/platform.service';

@Component({
  selector: 'app-games',
  templateUrl: './games.component.html',
  styleUrls: ['./games.component.css']
})
export class GamesComponent implements OnInit {

  constructor(
    private activatedRoute:ActivatedRoute,
    private matchService:MatchService,
    private platformService:PlatformService,
    private gamesService:GamesService) {
   }

   matches = []
   platform
   game

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {
      this.gamesService.getGame(params.game).subscribe((resp:any)=>{
         this.game = resp
        })
      this.matchService.getPlatformGameMatches(params.game, params.platform).subscribe((resp:[] ) => {
        this.matches = resp
        this.getPlatformInfo(params.platform)
        this.loadMatches()
      })
    })
  }

  loadMatches(){
    if (this.matches.length == 0){
      return
    }
    this.matches.forEach(element => {
      console.log(element)
      element.date = new Date(element.date)
    });
  }

  getPlatformInfo(platform){
    this.platformService.getPlatformInfo(platform).subscribe(resp =>{
      this.platform = resp
    })
  }

  // getGame(id){
  //   this.gamesService.getGame()
  // }
}
