import { Component, OnInit,  } from '@angular/core';
import { async } from 'rxjs';
import { MatchService } from 'src/app/services/match.service';
import { UserService } from 'src/app/services/user.service';

import { Platform } from "../../models/platform_games.model";

declare function customInitFunctions()

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  platforms = []
  
  user

  platform_games
  loadedGames = false

  constructor (private userService:UserService, private matchService:MatchService) { 

    if(localStorage.getItem('token')){
     this.userService.validateToken().subscribe(() =>{

      this.user = userService.user

      userService.userPlatforms().subscribe(platforms_resp =>{

        // this.platforms = platforms_resp
        customInitFunctions()
        this.loadTopGames(platforms_resp)

      })
     })

    }
  }

  resizeGameImage(game){
    
    console.log(game);
    let image = game.background_image.split('/')
    if(image[image.length -3] == "screenshots"){
      game.background_image = `https://media.rawg.io/media/crop/600/400/screenshots/${image[image.length - 2]}/${image[image.length - 1]}`
    }else{
      game.background_image = `https://media.rawg.io/media/crop/600/400/games/${image[image.length - 2]}/${image[image.length - 1]}`
    }
  
  }

  ngOnInit(): void {  
    customInitFunctions()
  }

 loadTopGames = async (platforms)=>{

  
  
 
  platforms.forEach (async element => {

    var platform_obj = new Platform(element.name, element.api_id, element.category, [])
    
    this.matchService.getMatchesByPlatform(element.api_id).subscribe((resp:any) => {
        if(resp.length !== 0 ){
          this.platform_games = resp
          this.platform_games.forEach(async elemento => {
            await elemento.platforms.forEach(plataforma => {
              if(plataforma.platform_id == element.api_id && plataforma.amount >= 1){
                this.resizeGameImage(elemento)
                platform_obj.push_game({name: elemento.name, amount: plataforma.amount, img: elemento.background_image, id: elemento.game_id})
              }
            });
            
          });
        }
        this.platforms.push(platform_obj)
      })
      
    });

  }
  getGamesOfPlatform(platform_id){
    this.matchService.getMatchesByPlatform(platform_id).subscribe(resp => resp)
  }

}
