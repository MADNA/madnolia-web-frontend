import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';


declare function customInitFunctions();
declare function toastMessage(message, color)
@Component({
  selector: 'app-partners',
  templateUrl: './partners.component.html',
  styleUrls: ['./partners.component.css']
})
export class PartnersComponent implements OnInit {

  keyPressed = 0
  foundedUsers:Array<Object> = null
  partners:Array<string> = []

  constructor(private userService:UserService) {
    customInitFunctions()
    userService.getPartners().subscribe((resp:Array<string>)=>{
      this.partners = resp.reverse()
      console.log(this.partners)
    })
   }

  ngOnInit(): void {
  }

  searchUser = (value) =>{

    this.keyPressed++
  
    setTimeout(()=>{
      this.keyPressed --
      if (this.keyPressed == 0){
        this.userService.searchPartners(value).subscribe((resp:any) =>{
          this.foundedUsers = resp.userDB
        },(err) => console.log(err)) 
      }

    }, 500)

  }
  addPartner(partner){
    let body ={
      partner
    }
    this.userService.addPartner(body).subscribe((resp:any) =>{
      if(resp.message){
          if(resp.ok){
            toastMessage(resp.message, "green darken-1")
            this.userService.getPartners().subscribe((update:any) =>{
              this.partners = update
            })
          }else{
            toastMessage(resp.message, "red accent-4")
          }
      }
    })
  }
}
