import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { UserComponent } from './user/user.component';
import { MatchesComponent } from './matches/matches.component';
import { PartnersComponent } from './partners/partners.component';
import { GamesComponent } from './games/games.component';
import { ProfilePageComponent } from './profile-page/profile-page.component';
import { ProfileComponent } from './profile.component';
import { PlatformsComponent } from './platforms/platforms.component';



const routes: Routes = [

    {path:'', component: ProfileComponent, children:[

        {path: '', component: ProfilePageComponent},
        { path: 'user', component:UserComponent },
        { path: 'matches', component: MatchesComponent },
        { path: 'games', component: GamesComponent },
        { path: 'partners', component: PartnersComponent },
        { path: 'platforms', component: PlatformsComponent}
    ]}

];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ProfileRoutingModule {}
