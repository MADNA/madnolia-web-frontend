import { Component, OnInit } from '@angular/core';
import { Match } from 'src/app/models/match.model';
import { MatchService } from 'src/app/services/match.service';

@Component({
  selector: 'app-matches',
  templateUrl: './matches.component.html',
  styleUrls: ['./matches.component.css']
})
export class MatchesComponent implements OnInit {

  matches = []
  verify:boolean = false

  constructor(private matchService:MatchService) { 
    matchService.getPlayersMatches(0).subscribe((resp:any) =>{
      this.matches = resp.matches

      this.matches.forEach(element => {
        element.date = new Date(element.date)
      });

      if(this.matches.length === 0){
        this.verify = true
      }
    })
  }

  ngOnInit(): void {
  }

  deleteUserMatch(id){
    this.matchService.deleteMatch(id).subscribe((resp) =>{
    for (let index = 0; index < this.matches.length; index++) {
      const element = this.matches[index];
      if(element._id === id){
        this.matches.splice(index, 1)
        return
      }
      
    }
  })}
}
