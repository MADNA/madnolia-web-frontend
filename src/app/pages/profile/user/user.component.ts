import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { User } from 'src/app/models/user.model';
import { FileUploadService } from 'src/app/services/file-upload.service';
import { UserService } from 'src/app/services/user.service';

declare function customInitFunctions();
declare function toastMessage(message, color)
declare function selectInit()

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})


export class UserComponent implements OnInit {

  user = this.userService.user


  editForm = this.fb.group({
    name: [this.user.name, [Validators.required, Validators.minLength(3), Validators.maxLength(20), Validators.pattern(/^[a-zA-Z]+$/)]],
    username: [this.user.username, [Validators.required, Validators.minLength(2), Validators.maxLength(12), Validators.pattern(/^[a-zA-Z0-9_\-¡!]+$/)]],
    email: [this.user.email, [Validators.required, Validators.email]],
    acceptInvitations: [this.user.acceptInvitations, Validators.required]
  })

  constructor (
    private http:HttpClient,
    private userService:UserService,
    private fb:FormBuilder,
    private fileUploadService:FileUploadService,
    private translate:TranslateService) { 
    customInitFunctions()
    // this.loadInfo()
   this.user = userService.user
  //  this.initForm()
  }
  
  initForm(){
    // this.editForm = this.fb.group({
    //   name: [this.user.name, [Validators.required, Validators.min(1), Validators.max(10)]],
    //   username: [this.user.username, [Validators.required, Validators.min(3), Validators.max(10)]],
    //   email: [this.user.email, [Validators.required, Validators.email]],
    //   acceptInvitations: [this.user.acceptInvitations, Validators.required]
    // })
    // selectInit()
  }
  
  ngOnInit(): void {
    if(localStorage.getItem('token')){
      this.userService.validateToken().subscribe(resp =>{
        this.user = this.userService.user
      })
    }
  }

  verLetra(value){
    console.log(this.editForm.controls)
  }


  updateUser(){
    if(this.editForm.invalid){
      this.translate.get('PROFILE.USER_PAGE.ERRORS.VERIFY').subscribe(resp =>{
        toastMessage(resp, 'red accent-4')
      })
      return
    }
    this.userService.updateUser(this.editForm.value).subscribe((resp:any) =>{
      this.translate.get(resp.message).subscribe(msg=>{
        toastMessage(msg, "green darken-1")
      })
    }, (err) => {
      if (err.error.err.keyPattern.email){
        this.translate.get('PROFILE.USER_PAGE.ERRORS.EMAIL').subscribe(resp =>{
          toastMessage(resp, "red accent-4")
        })
      }
      else if(err.error.err.keyPattern.username){
        this.translate.get('PROFILE.USER_PAGE.ERRORS.USERNAME').subscribe(msg =>{
          toastMessage(msg, "red accent-4")
        })
      }
    })
  }


  changeImg(img:File){
    if(img.size > 2000000){
      this.translate.get('PROFILE.USER_PAGE.ERRORS.IMG_SIZE').subscribe(msg =>{
        return toastMessage(msg, "red accent-4")
      })
    }

    const extension = img.name.split('.').slice(-1).toString()
    // Validate extension
    const validExtensions = ['png', 'jpg', 'jpeg']

    if(!validExtensions.includes(extension)){
      this.translate.get('PROFILE.USER_PAGE.ERRORS.IMG_EXTENSION').subscribe(msg =>{
        return toastMessage(msg, "red accent-4")
      })
    }

    this.translate.get('PROFILE.USER_PAGE.UPLOADING_IMG').subscribe(msg =>{
      toastMessage(msg, "blue darken-4")
    })
  
    this.fileUploadService.updateImg(img, 'users', this.user.uid)
    .then(imgDB =>{ 
      if(imgDB.ok){
        this.userService.user.img = imgDB.img
      }
    }).catch(err => {
      this.translate.get('PROFILE.USER_PAGE.ERRORS.IMG_UPDATING').subscribe(msg =>{
        toastMessage(msg, "red accent-4")
      })
    })
    
  }

}