import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Socket } from 'ngx-socket-io';
import { GamesService } from 'src/app/services/games.service';
import { MatchService } from 'src/app/services/match.service';
import { UserService } from 'src/app/services/user.service';
import { environment } from "../../../environments/environment";
import { TranslateService } from '@ngx-translate/core';

declare function customInitFunctions();
declare function dateInit(event);
declare function toastMessage(message, color)
declare function openGamesModal()
declare function closeModal()
declare function resetScroll()

@Component({
  selector: 'app-create-match',
  templateUrl: './create-match.component.html',
  styleUrls: ['./create-match.component.css']
})
export class CreateMatchComponent implements OnInit {
  
  matchDate:any = ""
  matchHour
  user
  users = []
  invitedUsers = []
  invitedUsers_ = []
  platforms = null
  platformsLoaded
  platformIco:String
  platformCategory:String
  platformCategoryIco:String
  platformName:String = null
  platformId:Number 
  emptyPlatforms:boolean = false
  url = environment.base_url
  
  games:Array<string>
  gameName:String  = ""
  gameImg:String
  gameId:Number = 0
  
  actualPage = 0

  keyPressed = 0
  gameSearch = null
  
  constructor( private fb:FormBuilder,
    private userService:UserService,
    private gamesService:GamesService,
    private matchService:MatchService,
    private router:Router, 
    private transleService:TranslateService,
    private socket:Socket) { 
    customInitFunctions()
    this.loadInfo()
    this.loadPlatforms()
  }
  
  match = this.fb.group({
    name: ['Casual Match', [Validators.required, Validators.max(20)]],
    date: [""],
    game_name: [this.gameName, [Validators.required]],
    game_id: [this.gameId, [Validators.required]],
    platform:[''],
    img: ['', ],
    users: [this.invitedUsers]
  })

  createMatch(){

    if(!this.matchDate || !this.matchHour){
      this.transleService.get('CREATE_MATCH.DATE_ERROR').subscribe(resp =>{
        toastMessage(resp, "red accent-4")
      })
      return
    }
    
    this.matchDate.setHours(this.matchHour[0])
    this.matchDate.setMinutes(this.matchHour[1])

    this.match.value.date = this.matchDate.getTime()
    this.match.value.platform = this.platformId
    this.match.value.game_name = this.gameName
    this.match.value.game_id = this.gameId
    this.match.value.img = this.gameImg

    this.matchService.createMatch(this.match.value).subscribe((resp:any) =>{
      this.socket.emit('match_created', resp.matchDB)
      this.transleService.get('CREATE_MATCH.MATCH_CREATED').subscribe(msg =>{
        toastMessage(msg, "green")
        this.router.navigateByUrl('/profile/matches')
      })
    }, (err) => console.log(err))

  }

  onDateChange(event){
    this.matchDate = new Date(event.firedBy.date)
    let fecha = new Date()
    if (!this.matchHour){
      return
    }
    this.matchDate.setHours(this.matchHour[0])
    this.matchDate.setMinutes(this.matchHour[1])
    
    resetScroll()
  }

  onTimeChange(event){
    this.matchHour = event.target.value.split(':')
    
    resetScroll()
  }

  ngOnInit(): void {
    customInitFunctions()
  }

  loadInfo = async() => {
    this.user = await this.userService.userInfo().subscribe( resp => {
      this.user = resp
    }, err => console.log(err))
  }

  loadPlatforms = async () =>{
    this.userService.userPlatforms().subscribe( resp => {
      this.platforms = resp
      if (this.platforms.length == 0){
        this.emptyPlatforms = true
      }
    }, err => console.log(err))
  }

  loadGames() {
    this.gamesService.getGamesByPlatform(this.platformId, '1').subscribe( (resp:any) => {
      this.games = resp
    }, (err) => console.log(err))
  }

  getPlatform(name, category, id, ico) {
    
    
    this.platformName = name
    this.platformCategory = category
    this.platformId = id
    this.platformIco = ico
    switch (this.platformCategory) {
      case 'PlayStation':
        this.platformCategoryIco = "icon-playstation"
        break;
      case 'Xbox':
        this.platformCategoryIco = "icon-xbox"
        break;
      case 'Nintendo':
        this.platformCategoryIco = "icon-nintendo"
      case "Mobile":
        this.platformCategoryIco = "icon-smartphone large"
        break;
      case "PC":
        this.platformCategoryIco = "icon-pc large"
      default:
        break;
    }
    this.platformsLoaded = this.platforms
    this.platforms = null
    this.emptyPlatforms = false
    dateInit(event)
    
  }

  backToSelect() {
    
    openGamesModal()
    this.platforms = this.platformsLoaded
    this.platformId = null
    this.platformName = null
    document.getElementById("formulario").hidden = true
    document.getElementById("gameSelector").hidden = false
  }

  searchGame = async(value) =>{

    this.keyPressed++
    setTimeout(()=>{
      this.keyPressed --
      if (this.keyPressed == 0){
        this.gameSearch = value
        this.gamesService.searchGamesByPlatform(value, this.platformId).subscribe((resp:any) =>{
          openGamesModal()
          this.games = resp.results
          this.gamesService.resizeImages(this.games)
          this.gameSearch = null
        }, (err) => console.log(err))
      }
    }, 1500)

  }

  selectGame(id, name, img){
    this.gameName = name
    this.gameId = id
    this.gameImg = img
    document.getElementById("formulario").hidden = false
    this.games = []
    document.getElementById("gameSelector").hidden = true
    closeModal()
  }

  searchUser(username){
    this.keyPressed++

    setTimeout(() => {
      this.keyPressed--
      if (this.keyPressed == 0){
        if(username.value == ""){
          this.users = []
          return
        }
        this.userService.searchPartners(username.value).subscribe((resp:any) =>{
          console.log(resp);
          this.users = []
          for (let index = 0; index < resp.length; index++) {
            const element = resp[index];
        
            if(element._id == this.user.uid || this.invitedUsers.includes(element._id)){
              continue
            }else{
              this.users.push(element)
            }
          }
          
        })
      }
    }, 1000)
  }
  addUser(user){
    this.invitedUsers.push(user._id)
    this.invitedUsers_.push(user.username)
    this.users = []
  }
}
