import { Component, OnDestroy, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-presentation',
  templateUrl: './presentation.component.html',
  styleUrls: ['./presentation.component.css']
})
export class PresentationComponent implements OnInit, OnDestroy {

  constructor(translate:TranslateService) { 
    document.getElementsByTagName('body')[0].style.background = "linear-gradient(to right, rgb(90, 0, 0), rgba(0, 0, 80), black)";
  }
  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    document.getElementsByTagName('body')[0].style.background = "linear-gradient(90deg, rgb(0, 20, 197), rgb(138, 0, 0))"

  }

}
