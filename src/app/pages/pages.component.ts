import { Component, OnInit } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { UserService } from '../services/user.service';
declare function customInitFunctions()
declare function toastWithAction(html)
@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styles: [
  ]
})
export class PagesComponent implements OnInit {

  constructor(private socket:Socket, private userService:UserService) { }

  ngOnInit(): void {
    if(localStorage.getItem('token')){
      this.socket.emit('init_user', localStorage.getItem('token'))
      this.socket.on('notification', (data) =>{
        this.userService.user.notifications += 1
        toastWithAction(`<p><span class="blue-text text-lighten-2">${data.name}</span> te ha invitado a una partida</p><a class="btn-flat toast-action" href=${data.match_url}>Unirme</a>`)    
      })
    }
    customInitFunctions()
    // toastWithAction('<p><span class="blue-text text-lighten-2">MADNA</span> te ha invitado a una partida</p><a class="btn-flat toast-action" href="/platforms">Unirme</a>')
  }

  

}
