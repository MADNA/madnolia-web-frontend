export interface Match{
    active:boolean,
    date: number,
    game_id:number,
    game_name:string,
    likes:Array<string>,
    message:string,
    platform:number,
    user:string,
    _id:string,
}