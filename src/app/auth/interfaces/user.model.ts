import { environment } from "../../../environments/environment";

const base_url = environment.base_url

export class User{

    constructor(
        public name: string,
        public username: string,
        public email: string,
        public platforms: Array<any>,
        public acceptInvitations: string,
        public uid: string,
        public img?:string
    ){}
    get imgUrl(){

        if(this.img.includes('http')){
            return this.img
        }

        if(this.img){
            return `${base_url}/upload/users/${this.img}`
        }else{
            return ""
        }
    }    

}