export interface RegisterForm{
    name:String;
    username:String;
    email:String;
    password:String;
    password2:String;
    platforms:Array<string>
}