import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { UserService } from 'src/app/services/user.service';

declare function customInitFunctions()
declare function toastMessage(message, color)

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private fb:FormBuilder,
    private userService:UserService,
    private router:Router,
    private translate:TranslateService,) { }

  loginForm = this.fb.group({
    username: ['', Validators.required],
    password: ['', Validators.required],
    remember: [false]
  })

  ngOnInit(): void {
    customInitFunctions()
  }

  login(){

    this.userService.login(this.loginForm.value).subscribe((resp:any) =>{
      this.router.navigateByUrl('/home')
    }, (err:any) => {
      if(err.error.message){
        this.translate.get(err.error.message).subscribe(resp =>{
          toastMessage(resp, "red accent-4")
        })
      }else{
        console.log(err)
      }
    })

  }

}
