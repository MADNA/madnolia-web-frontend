import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { PlaystationComponent } from '../pages/platforms/playstation/playstation.component';
import { XboxComponent } from '../pages/platforms/xbox/xbox.component';
import { NintendoComponent } from '../pages/platforms/nintendo/nintendo.component';


const routes: Routes = [
    {path: 'login', component: LoginComponent},
    {path: 'register', component: RegisterComponent, children:[
        {path: 'playstation', component: PlaystationComponent},
        {path: 'xbox', component: XboxComponent},
        {path: 'nintendo', component: NintendoComponent}
    ]},
    {path: '**', redirectTo: ''}
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AuthRoutingModule {}
