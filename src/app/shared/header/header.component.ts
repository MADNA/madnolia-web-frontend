import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Socket } from 'ngx-socket-io';
import { environment } from 'src/environments/environment';

import { UserService } from 'src/app/services/user.service';
import { TranslateService } from '@ngx-translate/core';
declare function customInitFunctions()
declare function navbarTrigger()
declare function closeNavbar()

const base_url = environment.base_url

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  isLoged:boolean = false
  isIndex:boolean = false
  user

  
  constructor(private router:Router, private userService:UserService, private socket:Socket, translate:TranslateService) { 
    
    translate.addLangs(['es', 'en']);
    translate.setDefaultLang('es');

    const browserLang = translate.getBrowserLang();
    // translate.use(browserLang.match(/en|es|fr/) ? browserLang : 'en');
  
  }

  verifyLogin = () =>{
    if(localStorage.getItem('token')){
      return true
    }else{
      this.isIndex = false
      return false
    }
   }

   
   

  ngOnInit(): void {
    customInitFunctions()
    navbarTrigger()
    if(localStorage.getItem('token')){
      this.userService.validateToken().subscribe(resp =>{
        this.user = this.userService.user
      })
    }
    
  }

  closeNav(){
    closeNavbar()
    if(localStorage.getItem('token')){
      navbarTrigger()
    }
  }

  logout = () => {
    localStorage.removeItem('token')
    this.router.navigateByUrl('/')
  }

}
