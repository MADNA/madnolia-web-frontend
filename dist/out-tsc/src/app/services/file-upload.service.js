import { __awaiter, __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { environment } from "../../environments/environment";
const base_url = environment.base_url;
let FileUploadService = class FileUploadService {
    constructor() { }
    updateImg(file, type, id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const url = `${base_url}/upload/${type}`;
                const formData = new FormData();
                formData.append('img', file);
                const resp = yield fetch(url, {
                    method: 'PUT',
                    headers: {
                        'token': localStorage.getItem('token') || ''
                    },
                    body: formData
                });
                const data = yield resp.json();
                return data;
            }
            catch (error) {
                return error;
            }
        });
    }
};
FileUploadService = __decorate([
    Injectable({
        providedIn: 'root'
    })
], FileUploadService);
export { FileUploadService };
//# sourceMappingURL=file-upload.service.js.map