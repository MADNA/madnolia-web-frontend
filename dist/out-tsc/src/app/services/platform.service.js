import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
const base_url = environment.base_url;
let PlatformService = class PlatformService {
    constructor(http) {
        this.http = http;
    }
    getPlatformInfo(platform) {
        return this.http.get(`${base_url}/get_platform/${platform}`);
    }
};
PlatformService = __decorate([
    Injectable({
        providedIn: 'root'
    })
], PlatformService);
export { PlatformService };
//# sourceMappingURL=platform.service.js.map