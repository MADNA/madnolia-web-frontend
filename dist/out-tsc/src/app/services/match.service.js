import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
const api_key = "8af7cb7fc9d949acac94ab83be57ed1b";
const base_url = environment.base_url;
const api_url = "https://api.rawg.io/api";
let MatchService = class MatchService {
    constructor(http) {
        this.http = http;
    }
    getMostPlatformGames(platform) {
        return this.http.get(`https://api.rawg.io/api/games?key=${api_key}&platforms=${platform}&tags=online`);
    }
    getPlayersMatches(_skip) {
        return this.http.get(`${base_url}/player_matches`, {
            headers: {
                "token": localStorage.getItem('token')
            },
            params: {
                skip: _skip
            }
        });
    }
    createMatch(match) {
        return this.http.post(`${base_url}/match`, match, {
            headers: {
                "token": localStorage.getItem('token')
            }
        });
    }
    deleteMatch(id) {
        return this.http.delete(`${base_url}/delete_match/${id}`, {
            headers: {
                token: localStorage.getItem('token')
            }
        });
    }
    getMatch(id) {
        return this.http.get(`${base_url}/match/${id}`);
    }
    getMatchesByGame(game) {
        return this.http.get(`${base_url}/game_matches/${game}`);
    }
    getPlatformGameMatches(game, platform) {
        return this.http.get(`${base_url}/matches_of_game/${platform}/${game}`);
    }
    getMatchesByPlatform(platform) {
        return this.http.get(`${base_url}/matches_of/${platform}`)
            .pipe(map((resp) => resp.total));
    }
};
MatchService = __decorate([
    Injectable({
        providedIn: 'root'
    })
], MatchService);
export { MatchService };
//# sourceMappingURL=match.service.js.map