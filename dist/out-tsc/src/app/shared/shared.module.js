import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { TranslateModule } from '@ngx-translate/core';
let SharedModule = class SharedModule {
};
SharedModule = __decorate([
    NgModule({
        declarations: [HeaderComponent, FooterComponent],
        imports: [
            CommonModule,
            RouterModule,
            TranslateModule
        ],
        exports: [
            HeaderComponent,
            FooterComponent
        ]
    })
], SharedModule);
export { SharedModule };
//# sourceMappingURL=shared.module.js.map