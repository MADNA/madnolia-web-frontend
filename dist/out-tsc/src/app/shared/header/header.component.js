import { __decorate } from "tslib";
import { Component } from '@angular/core';
import { environment } from 'src/environments/environment';
const base_url = environment.base_url;
let HeaderComponent = class HeaderComponent {
    constructor(router, userService, socket, translate) {
        this.router = router;
        this.userService = userService;
        this.socket = socket;
        this.isLoged = false;
        this.isIndex = false;
        this.verifyLogin = () => {
            if (localStorage.getItem('token')) {
                return true;
            }
            else {
                this.isIndex = false;
                return false;
            }
        };
        this.logout = () => {
            localStorage.removeItem('token');
            this.router.navigateByUrl('/');
        };
        translate.addLangs(['es', 'en']);
        translate.setDefaultLang('es');
        const browserLang = translate.getBrowserLang();
        // translate.use(browserLang.match(/en|es|fr/) ? browserLang : 'en');
    }
    ngOnInit() {
        customInitFunctions();
        navbarTrigger();
        if (localStorage.getItem('token')) {
            this.userService.validateToken().subscribe(resp => {
                this.user = this.userService.user;
            });
        }
    }
    closeNav() {
        closeNavbar();
        if (localStorage.getItem('token')) {
            navbarTrigger();
        }
    }
};
HeaderComponent = __decorate([
    Component({
        selector: 'app-header',
        templateUrl: './header.component.html',
        styleUrls: ['./header.component.css']
    })
], HeaderComponent);
export { HeaderComponent };
//# sourceMappingURL=header.component.js.map