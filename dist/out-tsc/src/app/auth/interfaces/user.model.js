import { environment } from "../../../environments/environment";
const base_url = environment.base_url;
export class User {
    constructor(name, username, email, platforms, acceptInvitations, uid, img) {
        this.name = name;
        this.username = username;
        this.email = email;
        this.platforms = platforms;
        this.acceptInvitations = acceptInvitations;
        this.uid = uid;
        this.img = img;
    }
    get imgUrl() {
        if (this.img.includes('http')) {
            return this.img;
        }
        if (this.img) {
            return `${base_url}/upload/users/${this.img}`;
        }
        else {
            return "";
        }
    }
}
//# sourceMappingURL=user.model.js.map