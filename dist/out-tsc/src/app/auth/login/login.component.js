import { __decorate } from "tslib";
import { Component } from '@angular/core';
import { Validators } from '@angular/forms';
let LoginComponent = class LoginComponent {
    constructor(fb, userService, router, translate) {
        this.fb = fb;
        this.userService = userService;
        this.router = router;
        this.translate = translate;
        this.loginForm = this.fb.group({
            username: ['', Validators.required],
            password: ['', Validators.required],
            remember: [false]
        });
    }
    ngOnInit() {
        customInitFunctions();
    }
    login() {
        this.userService.login(this.loginForm.value).subscribe((resp) => {
            this.router.navigateByUrl('/home');
        }, (err) => {
            if (err.error.message) {
                this.translate.get(err.error.message).subscribe(resp => {
                    toastMessage(resp, "red accent-4");
                });
            }
            else {
                console.log(err);
            }
        });
    }
};
LoginComponent = __decorate([
    Component({
        selector: 'app-login',
        templateUrl: './login.component.html',
        styleUrls: ['./login.component.css']
    })
], LoginComponent);
export { LoginComponent };
//# sourceMappingURL=login.component.js.map