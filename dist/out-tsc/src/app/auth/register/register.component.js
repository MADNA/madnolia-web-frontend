import { __awaiter, __decorate } from "tslib";
import { Component } from '@angular/core';
import { FormArray, Validators, FormControl } from '@angular/forms';
let RegisterComponent = class RegisterComponent {
    constructor(fb, userService, router, translate, translateService) {
        this.fb = fb;
        this.userService = userService;
        this.router = router;
        this.translateService = translateService;
        this.selectPlayStation = false;
        this.selectNintendo = false;
        this.selectXbox = false;
        this.showPlatforms = false;
        this.playstationPlatforms = [
            { name: 'PlayStation 2', value: 15, class: "icon-dualshock-2 large" },
            { name: 'PlayStation 3', value: 16, class: "icon-dualshock-3 large" },
            { name: 'PlayStation 4', value: 18, class: "icon-dualshock-4 large" },
            { name: 'PlayStation Portable', value: 17, class: "icon-psp large" },
            { name: 'PlayStation Vita', value: 19, class: "icon-ps-vita large" },
            { name: 'PlayStation 5', value: 187, class: "icon-dualsense large" },
        ];
        this.nintendoPlatforms = [
            { name: "Wii", value: 11, class: 'icon-wii large' },
            { name: "WiiU", value: 10, class: 'icon-wiiu large' },
            { name: '3DS', value: 8, class: 'icon-3ds large' },
            { name: "Switch", value: 7, class: 'icon-nswitch large' }
        ];
        this.xboxPlatforms = [
            { name: "Classic", value: 80, class: "icon-xbox-classic large" },
            { name: "360", value: 14, class: "icon-360 large" },
            { name: "One", value: 1, class: "icon-xbox-one large" },
            { name: "Series S/X", value: 186, class: "icon-xbox-one large" }
        ];
        this.formSubmitted = false;
        this.inputForm = true;
        this.registerForm = this.fb.group({
            name: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(20), Validators.pattern(/^[a-zA-Z]+$/)]],
            username: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(12), Validators.pattern(/^[a-zA-Z0-9_\-¡!]+$/)]],
            email: ['', [Validators.required, Validators.email]],
            password: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(50)]],
            password2: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(50)]],
            platforms: new FormArray([])
        }, {
            validators: this.samePasswords('password', 'password2')
        });
        this.verifyRegisterForm = () => __awaiter(this, void 0, void 0, function* () {
            this.formSubmitted = true;
            const controls = this.registerForm.controls;
            if (this.registerForm.invalid) {
                return;
            }
            this.userService.verifyUserName(controls.username.value, controls.email.value).subscribe((resp) => {
                this.hideFormShowConsoles();
            }, (err) => {
                toastMessage(err.error.err.message, "red accent-4");
            });
            if (!controls.platforms.value) {
                console.log("Está vacio");
            }
        });
        this.showForm = () => this.inputForm;
    }
    ngOnInit() {
        customInitFunctions();
    }
    createUser() {
        this.formSubmitted = true;
        if (this.registerForm.invalid) {
            return;
        }
        this.userService.createUser(this.registerForm.value).subscribe(resp => {
            this.router.navigateByUrl('/home');
            this.translateService.get('REGISTER.TOAST_SIGN_UP', {}).subscribe(resp => {
                toastMessage(resp, "blue darken-1");
            });
        }, (err) => {
            if (err.error.errors.platforms.msg) {
                toastMessage(err.error.errors.platforms.msg, "red accent-4");
            }
        });
    }
    verifyCheckbox() {
        const platforms = this.registerForm.controls['platforms'];
        let elements = document.querySelectorAll("[id]");
        elements.forEach((elemento) => {
            if (platforms.value.includes(Number(elemento.id))) {
                elemento.setAttribute('style', "color: white");
            }
        });
    }
    onCheckboxChange(event) {
        const platforms = this.registerForm.controls['platforms'];
        // console.log(event)
        if (event.target.checked) {
            // event.path[2].firstChild.setAttribute('style', 'color: white')
            platforms.push(new FormControl(event.target.value));
        }
        else {
            // event.path[2].firstChild.setAttribute('style', 'color: black')
            const index = platforms.controls.findIndex(x => x.value === event.target.value);
            platforms.removeAt(index);
        }
    }
    onAddPlatform(platform) {
        const platforms = this.registerForm.controls['platforms'];
        // console.log(platform)
        if (platforms.value.includes(platform)) {
            const index = platforms.controls.findIndex(x => x.value === platform);
            platforms.removeAt(index);
            document.getElementById(platform).removeAttribute('style');
        }
        else {
            platforms.push(new FormControl(platform));
            document.getElementById(platform).setAttribute('style', 'color:white');
        }
    }
    noValidInput(input) {
        if (this.registerForm.get(input).invalid && this.formSubmitted) {
            return true;
        }
        else {
            return false;
        }
    }
    noValidPasswords() {
        const pass1 = this.registerForm.get('password').value;
        const pass2 = this.registerForm.get('password2').value;
        if ((pass1 !== pass2) && this.formSubmitted) {
            return true;
        }
        else {
            return false;
        }
    }
    samePasswords(pass1Name, pass2Name) {
        return (formGroup) => {
            const pass1Control = formGroup.get(pass1Name);
            const pass2Control = formGroup.get(pass2Name);
            if (pass1Control.value === pass2Control.value) {
                pass2Control.setErrors(null);
            }
            else {
                pass2Control.setErrors({ notTheSame: true });
            }
        };
    }
    hideFormShowConsoles() {
        this.inputForm = false;
        this.animate();
    }
    changePS() {
        this.selectPlayStation = true;
        this.selectNintendo = false;
        this.selectXbox = false;
    }
    changeNintendo() {
        this.selectNintendo = true;
        this.selectPlayStation = false;
        this.selectXbox = false;
    }
    changeXbox() {
        this.selectXbox = true;
        this.selectPlayStation = false;
        this.selectNintendo = false;
    }
    animate() {
        this.showPlatforms = true;
    }
};
RegisterComponent = __decorate([
    Component({
        selector: 'app-register',
        templateUrl: './register.component.html',
        styleUrls: ['./register.component.css']
    })
], RegisterComponent);
export { RegisterComponent };
//# sourceMappingURL=register.component.js.map