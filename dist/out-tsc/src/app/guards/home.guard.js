import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
let HomeGuard = class HomeGuard {
    constructor(userService, router) {
        this.userService = userService;
        this.router = router;
    }
    canActivate() {
        if (localStorage.getItem('token')) {
            this.router.navigateByUrl('/home');
        }
        else {
            return true;
        }
    }
};
HomeGuard = __decorate([
    Injectable({
        providedIn: 'root'
    })
], HomeGuard);
export { HomeGuard };
//# sourceMappingURL=home.guard.js.map