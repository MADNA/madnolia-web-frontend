import { environment } from "../../environments/environment";
const base_url = environment.base_url;
export class User {
    constructor(name, username, email, platforms, acceptInvitations, notifications, uid, img, thumb_img) {
        this.name = name;
        this.username = username;
        this.email = email;
        this.platforms = platforms;
        this.acceptInvitations = acceptInvitations;
        this.notifications = notifications;
        this.uid = uid;
        this.img = img;
        this.thumb_img = thumb_img;
    }
}
//# sourceMappingURL=user.model.js.map