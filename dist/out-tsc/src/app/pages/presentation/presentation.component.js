import { __decorate } from "tslib";
import { Component } from '@angular/core';
let PresentationComponent = class PresentationComponent {
    constructor(translate) {
        document.getElementsByTagName('body')[0].style.background = "linear-gradient(to right, rgb(90, 0, 0), rgba(0, 0, 80), black)";
    }
    ngOnInit() {
    }
    ngOnDestroy() {
        document.getElementsByTagName('body')[0].style.background = "linear-gradient(90deg, rgb(0, 20, 197), rgb(138, 0, 0))";
    }
};
PresentationComponent = __decorate([
    Component({
        selector: 'app-presentation',
        templateUrl: './presentation.component.html',
        styleUrls: ['./presentation.component.css']
    })
], PresentationComponent);
export { PresentationComponent };
//# sourceMappingURL=presentation.component.js.map