import { __decorate } from "tslib";
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { UserComponent } from './user/user.component';
import { MatchesComponent } from './matches/matches.component';
import { PartnersComponent } from './partners/partners.component';
import { GamesComponent } from './games/games.component';
import { ProfilePageComponent } from './profile-page/profile-page.component';
import { ProfileComponent } from './profile.component';
import { PlatformsComponent } from './platforms/platforms.component';
const routes = [
    { path: '', component: ProfileComponent, children: [
            { path: '', component: ProfilePageComponent },
            { path: 'user', component: UserComponent },
            { path: 'matches', component: MatchesComponent },
            { path: 'games', component: GamesComponent },
            { path: 'partners', component: PartnersComponent },
            { path: 'platforms', component: PlatformsComponent }
        ] }
];
let ProfileRoutingModule = class ProfileRoutingModule {
};
ProfileRoutingModule = __decorate([
    NgModule({
        imports: [RouterModule.forChild(routes)],
        exports: [RouterModule]
    })
], ProfileRoutingModule);
export { ProfileRoutingModule };
//# sourceMappingURL=profile.routing.js.map