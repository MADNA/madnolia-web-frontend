import { __decorate } from "tslib";
import { Component } from '@angular/core';
let PartnersComponent = class PartnersComponent {
    constructor(userService) {
        this.userService = userService;
        this.keyPressed = 0;
        this.foundedUsers = null;
        this.partners = [];
        this.searchUser = (value) => {
            this.keyPressed++;
            setTimeout(() => {
                this.keyPressed--;
                if (this.keyPressed == 0) {
                    this.userService.searchPartners(value).subscribe((resp) => {
                        this.foundedUsers = resp.userDB;
                    }, (err) => console.log(err));
                }
            }, 500);
        };
        customInitFunctions();
        userService.getPartners().subscribe((resp) => {
            this.partners = resp.reverse();
            console.log(this.partners);
        });
    }
    ngOnInit() {
    }
    addPartner(partner) {
        let body = {
            partner
        };
        this.userService.addPartner(body).subscribe((resp) => {
            if (resp.message) {
                if (resp.ok) {
                    toastMessage(resp.message, "green darken-1");
                    this.userService.getPartners().subscribe((update) => {
                        this.partners = update;
                    });
                }
                else {
                    toastMessage(resp.message, "red accent-4");
                }
            }
        });
    }
};
PartnersComponent = __decorate([
    Component({
        selector: 'app-partners',
        templateUrl: './partners.component.html',
        styleUrls: ['./partners.component.css']
    })
], PartnersComponent);
export { PartnersComponent };
//# sourceMappingURL=partners.component.js.map