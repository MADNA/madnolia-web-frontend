import { __decorate } from "tslib";
import { Component } from '@angular/core';
import { FormArray, FormControl } from '@angular/forms';
let PlatformsComponent = class PlatformsComponent {
    constructor(fb, userService, translate) {
        this.fb = fb;
        this.userService = userService;
        this.translate = translate;
        this.updatePlatforms = this.fb.group({
            platforms: new FormArray([])
        });
        this.selectPlayStation = false;
        this.selectNintendo = false;
        this.selectXbox = false;
        this.showPlatforms = false;
        this.inputForm = false;
        this.playstationPlatforms = [
            { name: 'Playstation 2', value: 15, class: "icon-dualshock-2 large" },
            { name: 'Playstation 3', value: 16, class: "icon-dualshock-3 large" },
            { name: 'Playstation 4', value: 18, class: "icon-dualshock-4 large" },
            { name: 'Playstation Portable', value: 17, class: "icon-psp large" },
            { name: 'Playstation Vita', value: 19, class: "icon-ps-vita large" },
            { name: 'Playstation 5', value: 187, class: "icon-dualsense large" },
        ];
        this.nintendoPlatforms = [
            { name: "Wii", value: 11, class: 'icon-wii large' },
            { name: "WiiU", value: 10, class: 'icon-wiiu large' },
            { name: '3DS', value: 8, class: 'icon-3ds large' },
            { name: "Switch", value: 7, class: 'icon-nswitch large' }
        ];
        this.xboxPlatforms = [
            { name: "Classic", value: 80, class: "icon-xbox-classic large" },
            { name: "360", value: 14, class: "icon-360 large" },
            { name: "One", value: 1, class: "icon-xbox-one large" },
            { name: "Series S/X", value: 186, class: "icon-xbox-one large" }
        ];
        customInitFunctions();
        userService.userPlatforms().subscribe((resp) => {
            let platforms = this.updatePlatforms.controls['platforms'];
            resp.forEach(element => {
                platforms.push(new FormControl(element.api_id));
            });
            this.verifyCheckbox();
        });
    }
    ngOnInit() {
        this.verifyCheckbox();
    }
    verifyCheckbox() {
        const platforms = this.updatePlatforms.controls['platforms'];
        let elements = document.querySelectorAll("[id]");
        elements.forEach((elemento) => {
            if (platforms.value.includes(Number(elemento.id))) {
                elemento.setAttribute('style', "color: white");
            }
        });
    }
    onCheckboxChange(event) {
        console.log("actualiza");
        const platforms = this.updatePlatforms.controls['platforms'];
        if (event.target.checked) {
            platforms.push(new FormControl(event.target.value));
        }
        else {
            const index = platforms.controls.findIndex(x => x.value === event.target.value);
            platforms.removeAt(index);
        }
    }
    onAddPlatform(platform) {
        const platforms = this.updatePlatforms.controls['platforms'];
        // console.log(platform)
        if (platforms.value.includes(platform)) {
            const index = platforms.controls.findIndex(x => x.value === platform);
            platforms.removeAt(index);
            document.getElementById(platform).removeAttribute('style');
        }
        else {
            platforms.push(new FormControl(platform));
            document.getElementById(platform).setAttribute('style', 'color:white');
        }
    }
    hideFormShowConsoles() {
        this.inputForm = false;
        this.showPlatforms = true;
    }
    changePS() {
        this.selectPlayStation = true;
        this.selectNintendo = false;
        this.selectXbox = false;
    }
    changeNintendo() {
        this.selectNintendo = true;
        this.selectPlayStation = false;
        this.selectXbox = false;
    }
    changeXbox() {
        this.selectXbox = true;
        this.selectPlayStation = false;
        this.selectNintendo = false;
    }
    updateUserPlatforms() {
        this.userService.updateUserPlatforms(this.updatePlatforms.value).subscribe((resp) => {
            this.translate.get(resp.message).subscribe(msg => {
                toastMessage(msg, "green");
            });
        }, (err => {
            toastMessage(err.error.errors.platforms.msg, "red accent-4");
        }));
    }
};
PlatformsComponent = __decorate([
    Component({
        selector: 'app-platforms',
        templateUrl: './platforms.component.html',
        styleUrls: ['./platforms.component.css']
    })
], PlatformsComponent);
export { PlatformsComponent };
//# sourceMappingURL=platforms.component.js.map