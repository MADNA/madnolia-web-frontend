import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { ProfileComponent } from "./profile.component";
import { UserComponent } from './user/user.component';
import { MatchesComponent } from './matches/matches.component';
import { PartnersComponent } from './partners/partners.component';
import { GamesComponent } from './games/games.component';
import { ProfilePageComponent } from './profile-page/profile-page.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PlatformsComponent } from './platforms/platforms.component';
let ProfileModule = class ProfileModule {
};
ProfileModule = __decorate([
    NgModule({
        declarations: [UserComponent,
            ProfileComponent,
            MatchesComponent,
            PartnersComponent,
            GamesComponent,
            ProfilePageComponent,
            PlatformsComponent
        ],
        imports: [
            CommonModule,
            RouterModule,
            FormsModule,
            ReactiveFormsModule,
            TranslateModule
        ],
        exports: [
            ProfileComponent,
            UserComponent,
            GamesComponent,
            MatchesComponent,
            GamesComponent,
            ProfilePageComponent,
            PlatformsComponent,
            TranslateModule
        ]
    })
], ProfileModule);
export { ProfileModule };
//# sourceMappingURL=profile.module.js.map