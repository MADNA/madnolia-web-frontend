import { __decorate } from "tslib";
import { Component } from '@angular/core';
let MatchesComponent = class MatchesComponent {
    constructor(matchService) {
        this.matchService = matchService;
        this.matches = [];
        this.verify = false;
        matchService.getPlayersMatches(0).subscribe((resp) => {
            this.matches = resp.matches;
            this.matches.forEach(element => {
                element.date = new Date(element.date);
            });
            if (this.matches.length === 0) {
                this.verify = true;
            }
        });
    }
    ngOnInit() {
    }
    deleteUserMatch(id) {
        this.matchService.deleteMatch(id).subscribe((resp) => {
            for (let index = 0; index < this.matches.length; index++) {
                const element = this.matches[index];
                if (element._id === id) {
                    this.matches.splice(index, 1);
                    return;
                }
            }
        });
    }
};
MatchesComponent = __decorate([
    Component({
        selector: 'app-matches',
        templateUrl: './matches.component.html',
        styleUrls: ['./matches.component.css']
    })
], MatchesComponent);
export { MatchesComponent };
//# sourceMappingURL=matches.component.js.map