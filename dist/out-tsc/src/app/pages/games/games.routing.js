import { __decorate } from "tslib";
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { GamesComponent } from './games.component';
const routes = [
    { path: '/:platform/:game', component: GamesComponent }
];
let GamesRoutingModule = class GamesRoutingModule {
};
GamesRoutingModule = __decorate([
    NgModule({
        imports: [RouterModule.forChild(routes)],
        exports: [RouterModule]
    })
], GamesRoutingModule);
export { GamesRoutingModule };
//# sourceMappingURL=games.routing.js.map