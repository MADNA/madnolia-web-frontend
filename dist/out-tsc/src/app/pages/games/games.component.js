import { __decorate } from "tslib";
import { Component } from '@angular/core';
let GamesComponent = class GamesComponent {
    constructor(activatedRoute, matchService, platformService, gamesService) {
        this.activatedRoute = activatedRoute;
        this.matchService = matchService;
        this.platformService = platformService;
        this.gamesService = gamesService;
        this.matches = [];
    }
    ngOnInit() {
        this.activatedRoute.params.subscribe(params => {
            this.gamesService.getGame(params.game).subscribe((resp) => {
                this.game = resp;
            });
            this.matchService.getPlatformGameMatches(params.game, params.platform).subscribe((resp) => {
                this.matches = resp;
                this.getPlatformInfo(params.platform);
                this.loadMatches();
            });
        });
    }
    loadMatches() {
        if (this.matches.length == 0) {
            return;
        }
        this.matches.forEach(element => {
            console.log(element);
            element.date = new Date(element.date);
        });
    }
    getPlatformInfo(platform) {
        this.platformService.getPlatformInfo(platform).subscribe(resp => {
            this.platform = resp;
        });
    }
};
GamesComponent = __decorate([
    Component({
        selector: 'app-games',
        templateUrl: './games.component.html',
        styleUrls: ['./games.component.css']
    })
], GamesComponent);
export { GamesComponent };
//# sourceMappingURL=games.component.js.map