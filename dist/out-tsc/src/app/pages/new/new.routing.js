import { __decorate } from "tslib";
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { NewComponent } from './new.component';
import { NewPageComponent } from './new-page/new-page.component';
import { CreateMatchComponent } from './match/create-match.component';
import { TournamentComponent } from './tournament/tournament.component';
const routes = [
    { path: '', component: NewComponent, children: [
            { path: '', component: NewPageComponent },
            { path: 'match', component: CreateMatchComponent },
            { path: 'tournament', component: TournamentComponent }
        ] }
];
let NewRoutingModule = class NewRoutingModule {
};
NewRoutingModule = __decorate([
    NgModule({
        imports: [RouterModule.forChild(routes)],
        exports: [RouterModule]
    })
], NewRoutingModule);
export { NewRoutingModule };
//# sourceMappingURL=new.routing.js.map