import { __decorate } from "tslib";
import { Component } from '@angular/core';
let PlatformsComponent = class PlatformsComponent {
    constructor(gamesService) {
        this.gamesService = gamesService;
    }
    ngOnInit() {
    }
};
PlatformsComponent = __decorate([
    Component({
        selector: 'app-platforms',
        templateUrl: './platforms.component.html',
        styleUrls: ['./platforms.component.css']
    })
], PlatformsComponent);
export { PlatformsComponent };
//# sourceMappingURL=platforms.component.js.map