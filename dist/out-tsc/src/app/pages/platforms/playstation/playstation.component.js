import { __decorate, __param } from "tslib";
import { Component, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
let PlaystationComponent = class PlaystationComponent {
    constructor(document) {
        this.document = document;
        this.link = this.document.location.href;
        this.urlParts = this.link.split('/');
        this.path = this.urlParts[this.urlParts.length - 2];
        this.showRegister = () => this.path == 'register';
        console.log(this.document.location.href);
    }
    ngOnInit() {
    }
    ngOnDestroy() {
    }
};
PlaystationComponent = __decorate([
    Component({
        selector: 'app-playstation',
        templateUrl: './playstation.component.html',
        styleUrls: ['./playstation.component.css']
    }),
    __param(0, Inject(DOCUMENT))
], PlaystationComponent);
export { PlaystationComponent };
//# sourceMappingURL=playstation.component.js.map