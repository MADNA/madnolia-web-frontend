import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
let PlaystationModule = class PlaystationModule {
};
PlaystationModule = __decorate([
    NgModule({
        declarations: [],
        imports: [
            CommonModule,
            BrowserModule
        ],
        exports: []
    })
], PlaystationModule);
export { PlaystationModule };
//# sourceMappingURL=playstation.module.js.map