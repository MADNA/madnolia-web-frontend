import { __decorate } from "tslib";
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { PlaystationComponent } from './playstation/playstation.component';
import { NintendoComponent } from './nintendo/nintendo.component';
import { XboxComponent } from './xbox/xbox.component';
import { PcComponent } from './pc/pc.component';
import { PhoneComponent } from './phone/phone.component';
import { CommonModule } from '@angular/common';
import { PlatformComponent } from './platform/platform.component';
const routes = [
    { path: 'playstation', children: [
            { path: '', component: PlaystationComponent },
            { path: ':platform', component: PlatformComponent }
        ] },
    { path: 'nintendo', children: [
            { path: '', component: NintendoComponent },
            { path: ':platform', component: PlatformComponent }
        ] },
    { path: 'xbox', children: [
            { path: '', component: XboxComponent },
            { path: ':platform', component: PlatformComponent }
        ] },
    { path: 'pc', component: PcComponent },
    { path: 'phone', component: PhoneComponent }
];
let PlatformsRoutingModule = class PlatformsRoutingModule {
};
PlatformsRoutingModule = __decorate([
    NgModule({
        imports: [RouterModule.forChild(routes), CommonModule],
        exports: [RouterModule]
    })
], PlatformsRoutingModule);
export { PlatformsRoutingModule };
//# sourceMappingURL=platforms.routing.js.map