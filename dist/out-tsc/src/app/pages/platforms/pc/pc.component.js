import { __awaiter, __decorate } from "tslib";
import { Component, ViewChild } from '@angular/core';
let PcComponent = class PcComponent {
    constructor(gamesService) {
        this.gamesService = gamesService;
        this.platformId = 4;
        this.games = [];
        this.page = 1;
        this.LoadGames = (page) => __awaiter(this, void 0, void 0, function* () {
            yield this.gamesService.getGamesByPlatform(this.platformId, page).subscribe((resp) => {
                this.gamesService.resizeImages(resp.results);
                resp.results.forEach(element => {
                    this.games.push(element);
                });
                this.page += 1;
            }, (err) => {
                console.log(err);
            });
        });
        this.LoadGames(this.page);
    }
    ngOnInit() {
    }
    verScroll(value) {
        // console.log(value)
        let pos = this.gamesElement.nativeElement.scrollTop + this.gamesElement.nativeElement.offsetHeight;
        let max = this.gamesElement.nativeElement.scrollHeight;
        if (pos >= max) {
            this.LoadGames(this.page);
        }
    }
};
__decorate([
    ViewChild('gamesContainer')
], PcComponent.prototype, "gamesElement", void 0);
PcComponent = __decorate([
    Component({
        selector: 'app-pc',
        templateUrl: './pc.component.html',
        styleUrls: ['./pc.component.css']
    })
], PcComponent);
export { PcComponent };
//# sourceMappingURL=pc.component.js.map