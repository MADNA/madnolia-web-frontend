import { __decorate, __param } from "tslib";
import { DOCUMENT } from '@angular/common';
import { Component, Inject } from '@angular/core';
let XboxComponent = class XboxComponent {
    constructor(document) {
        this.document = document;
    }
    ngOnInit() { }
};
XboxComponent = __decorate([
    Component({
        selector: 'app-xbox',
        templateUrl: './xbox.component.html',
        styleUrls: ['./xbox.component.css']
    }),
    __param(0, Inject(DOCUMENT))
], XboxComponent);
export { XboxComponent };
//# sourceMappingURL=xbox.component.js.map