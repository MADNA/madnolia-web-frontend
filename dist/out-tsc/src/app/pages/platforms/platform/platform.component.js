import { __awaiter, __decorate } from "tslib";
import { Component, ViewChild } from '@angular/core';
let PlatformComponent = class PlatformComponent {
    constructor(activatedRoute, gamesService, platformsService) {
        this.activatedRoute = activatedRoute;
        this.gamesService = gamesService;
        this.platformsService = platformsService;
        this.games = [];
        this.page = 1;
        this.LoadGames = (page) => __awaiter(this, void 0, void 0, function* () {
            yield this.gamesService.getGamesByPlatform(this.platformId, page).subscribe((resp) => {
                this.gamesService.resizeImages(resp.results);
                resp.results.forEach(element => {
                    this.games.push(element);
                });
                this.page += 1;
            }, (err) => {
                console.log(err);
            });
        });
        this.activatedRoute.params.subscribe(params => {
            this.platformId = params.platform;
            this.platformsService.getPlatformInfo(params.platform).subscribe((resp) => {
                this.platformName = resp.name;
            });
        });
    }
    ngOnInit() {
        this.LoadGames(this.page);
    }
    verScroll(value) {
        // console.log(value)
        let pos = this.gamesElement.nativeElement.scrollTop + this.gamesElement.nativeElement.offsetHeight;
        let max = this.gamesElement.nativeElement.scrollHeight;
        if (pos >= max) {
            this.LoadGames(this.page);
        }
    }
};
__decorate([
    ViewChild('gamesContainer')
], PlatformComponent.prototype, "gamesElement", void 0);
PlatformComponent = __decorate([
    Component({
        selector: 'app-platform',
        templateUrl: './platform.component.html',
        styleUrls: ['./platform.component.css']
    })
], PlatformComponent);
export { PlatformComponent };
//# sourceMappingURL=platform.component.js.map