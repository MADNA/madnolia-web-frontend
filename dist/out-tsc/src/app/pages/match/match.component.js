import { __decorate } from "tslib";
import { Component, Injectable, ViewChild } from '@angular/core';
let MatchComponent = class MatchComponent {
    constructor(matchService, activatedRoute, platformService, socket) {
        this.matchService = matchService;
        this.activatedRoute = activatedRoute;
        this.platformService = platformService;
        this.socket = socket;
        this.loading = true;
        this.activatedRoute.params.subscribe(params => {
            this.matchService.getMatch(params.match).subscribe((resp) => {
                this.match = resp;
                this.socket.emit('init_match_chat', resp._id);
                this.socket_inputs();
                this.match.date = new Date(this.match.date);
                this.platformService.getPlatformInfo(this.match.platform).subscribe(platform => {
                    this.platform = platform;
                    this.loading = false;
                    this.messages = resp.chat;
                    this.scrollToBottom();
                });
            }, (err) => console.error(err));
        });
    }
    ngOnInit() {
    }
    ngOnDestroy() {
        this.socket.emit('disconnect_chat');
    }
    socket_inputs() {
        this.socket.on('message', (message) => {
            
            this.messages.push(message);
            this.scrollToBottom();
        });
    }
    scrollToBottom() {
        try {
            setTimeout(() => {
                this.container.nativeElement.scrollTop = this.container.nativeElement.scrollHeight;
            }, 10);
        }
        catch (err) {
            console.log(err);
        }
    }
    onSubmit(event) {
        if (event) {
            if (event.key != "Enter") {
                return;
            }
            else {
                event.preventDefault();
            }
        }
        if (this.message === "" || !this.message) {
            return;
        }
        this.socket.emit('message', this.message);
        this.message = null;
    }
};
__decorate([
    ViewChild('container', { static: false })
], MatchComponent.prototype, "container", void 0);
MatchComponent = __decorate([
    Component({
        selector: 'app-match',
        templateUrl: './match.component.html',
        styleUrls: ['./match.component.css']
    }),
    Injectable()
], MatchComponent);
export { MatchComponent };
//# sourceMappingURL=match.component.js.map