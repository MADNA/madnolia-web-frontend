import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlatformsModule } from './platforms/platforms.module';
import { SharedModule } from '../shared/shared.module';
import { ProfileModule } from './profile/profile.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GamesComponent } from './games/games.component';
import { PagesComponent } from './pages.component';
import { HomeComponent } from './home/home.component';
import { RouterModule } from '@angular/router';
import { CreateMatchComponent } from './create-match/create-match.component';
import { HttpClientModule } from '@angular/common/http';
import { MatchComponent } from './match/match.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { UserHomeComponent } from './user-home/user-home.component';
import { PresentationComponent } from './presentation/presentation.component';
import { NewModule } from './new/new.module';
let PagesModule = class PagesModule {
};
PagesModule = __decorate([
    NgModule({
        declarations: [GamesComponent, PagesComponent, HomeComponent, CreateMatchComponent, MatchComponent, NotificationsComponent, UserHomeComponent, PresentationComponent],
        imports: [
            CommonModule,
            SharedModule,
            RouterModule,
            PlatformsModule,
            ProfileModule,
            FormsModule,
            ReactiveFormsModule,
            HttpClientModule,
            NewModule
        ],
        exports: [
            GamesComponent,
            PagesComponent
        ]
    })
], PagesModule);
export { PagesModule };
//# sourceMappingURL=pages.module.js.map