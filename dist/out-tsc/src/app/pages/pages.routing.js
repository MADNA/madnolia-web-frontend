import { __decorate } from "tslib";
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { PlatformsRoutingModule } from './platforms/platforms.routing';
import { AuthGuard } from '../guards/auth.guard';
import { HomeGuard } from '../guards/home.guard';
import { PagesComponent } from './pages.component';
import { GamesComponent } from './games/games.component';
import { PlatformsComponent } from './platforms/platforms.component';
import { ProfileComponent } from './profile/profile.component';
import { CreateMatchComponent } from './create-match/create-match.component';
import { HomeComponent } from './home/home.component';
import { MatchComponent } from './match/match.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { PresentationComponent } from './presentation/presentation.component';
import { NewComponent } from './new/new.component';
const routes = [
    { path: '', component: PagesComponent,
        children: [
            { path: '', component: PresentationComponent, canActivate: [HomeGuard] },
            { path: 'home', component: HomeComponent, canActivate: [AuthGuard] },
            { path: 'platforms', component: PlatformsComponent, loadChildren: () => import('./platforms/platforms.routing').then(m => m.PlatformsRoutingModule) },
            { path: 'notifications', component: NotificationsComponent },
            { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard], loadChildren: () => import('./profile/profile.routing').then(m => m.ProfileRoutingModule) },
            { path: 'create_match', component: CreateMatchComponent, canActivate: [AuthGuard] },
            { path: 'match/:match', component: MatchComponent, canActivate: [AuthGuard] },
            { path: 'game/:platform/:game', component: GamesComponent },
            { path: 'new', component: NewComponent, canActivate: [AuthGuard], loadChildren: () => import('./new/new.routing').then(m => m.NewRoutingModule) }
        ]
    }
];
let PagesRoutingModule = class PagesRoutingModule {
};
PagesRoutingModule = __decorate([
    NgModule({
        imports: [
            RouterModule.forChild(routes),
            PlatformsRoutingModule
        ],
        exports: [RouterModule]
    })
], PagesRoutingModule);
export { PagesRoutingModule };
//# sourceMappingURL=pages.routing.js.map