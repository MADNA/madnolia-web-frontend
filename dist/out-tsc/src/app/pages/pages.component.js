import { __decorate } from "tslib";
import { Component } from '@angular/core';
let PagesComponent = class PagesComponent {
    constructor(socket, userService) {
        this.socket = socket;
        this.userService = userService;
    }
    ngOnInit() {
        if (localStorage.getItem('token')) {
            this.socket.emit('init_user', localStorage.getItem('token'));
            this.socket.on('notification', (data) => {
                this.userService.user.notifications += 1;
                toastWithAction(`<p><span class="blue-text text-lighten-2">${data.name}</span> te ha invitado a una partida</p><a class="btn-flat toast-action" href=${data.match_url}>Unirme</a>`);
            });
        }
        customInitFunctions();
        // toastWithAction('<p><span class="blue-text text-lighten-2">MADNA</span> te ha invitado a una partida</p><a class="btn-flat toast-action" href="/platforms">Unirme</a>')
    }
};
PagesComponent = __decorate([
    Component({
        selector: 'app-pages',
        templateUrl: './pages.component.html',
        styles: []
    })
], PagesComponent);
export { PagesComponent };
//# sourceMappingURL=pages.component.js.map