import { __decorate } from "tslib";
import { Component } from '@angular/core';
let NotificationsComponent = class NotificationsComponent {
    constructor(userService) {
        this.userService = userService;
    }
    ngOnInit() {
        this.userService.getInvitations().subscribe(resp => {
            this.matches = resp;
            this.matches.forEach(element => {
                element.date = new Date(element.date);
            });
        });
        if (this.userService.user) {
            if (this.userService.user.notifications > 0) {
                this.userService.resetNofications().subscribe(() => this.userService.user.notifications = 0);
            }
        }
    }
};
NotificationsComponent = __decorate([
    Component({
        selector: 'app-notifications',
        templateUrl: './notifications.component.html',
        styleUrls: ['./notifications.component.css']
    })
], NotificationsComponent);
export { NotificationsComponent };
//# sourceMappingURL=notifications.component.js.map